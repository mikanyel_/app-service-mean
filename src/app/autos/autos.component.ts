import { Component, OnInit } from '@angular/core';
import { BasicService } from '../services/basic.service';

@Component({
  selector: 'app-autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.css'],
  providers: [BasicService]
})
export class AutosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

class Auto {
  constructor(
    public marca : string,
    public modelo:string,
    public anio:number,
    public version:string,    
  ){}
}
